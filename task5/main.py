### Алгоритмическая задача - O(1)

def reverse(a: int, b: int) -> dict:
    c = a  ### O(1)
    a = b  ### O(1)
    b = c  ### O(1)
    return {'a': a, 'b': b}


### Алгоритмическая задача - O(n), при том, что операции O(1), сложность увеличится от размера переданного размера листа

def reverse_list(list: list) -> list:
    new_list = []  ### O(1)
    i = len(list) - 1  ### O(1)

    while i >= 0:
        new_list.append(list[i])  ### O(1)
        i -= 1  ### O(1)

    return new_list


### Факториал

def factorial():
    a = int(input('Введите число для вычисления факториала: '))
    result = 1
    for i in range(a + 1):
        if i == 0:
            continue
        result *= i
    return print(f'Факториал числа {a} - {result}')


# factorial()


### Подсчёт вхождений

def counter():
    string: str = input('Введите строку:')
    substring: str = input('Введите подстроку:')
    if len(string) < len(substring):
        return f'Подстрока длиннее строки, ничего не получится'
    count = 0
    i = 0
    for char in string:
        if char == substring[0]:
            if string[i:i + len(substring)] == substring:
                count += 1
        i += 1

    return print(f'Подстрока {substring} входит в строку {string} - {count} раз')


# Задача по поиску медианы на leetcode.

def findMedianSortedArrays(nums1: list[int], nums2: list[int]) -> float:
    result_array = sorted(nums1 + nums2)
    if len(result_array) % 2 == 0:
        result_array = result_array[int(len(result_array)/2)-1:int(len(result_array)/2)+1]
        print(result_array)
        mediana = 0
        for i in result_array:
            mediana+=i
        return mediana/2


    else:
        result_array = result_array[int(len(result_array)/2)]
        return result_array

