import sqlalchemy as sq
from sqlalchemy.orm import declarative_base
import random, string

DSN = "postgresql+psycopg2://postgres:postgres@localhost:5433/postgres"
Base = declarative_base()


class User(Base):
    __tablename__ = "user"
    id = sq.Column(sq.Integer, primary_key=True)
    name = sq.Column(sq.Text, nullable=False)
    gender = sq.Column(sq.Text, nullable=False)


def randomword(length):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(length))


def create_tables(engine):
    Base.metadata.create_all(engine)


def drop_tables(engine):
    Base.metadata.drop_all(engine)


def fill_base(session, qty: int = 10):
    for i in range(qty):
        user = User(name=randomword(5), gender=randomword(1))
        session.add(user)
        session.commit()

