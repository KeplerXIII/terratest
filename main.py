import typing

import pydantic
import sqlalchemy as sq
from sqlalchemy.orm import sessionmaker
from data import DSN
from models import create_tables, drop_tables, fill_base, User
from fastapi import FastAPI

app = FastAPI()

engine = sq.create_engine(DSN)
drop_tables(engine)
create_tables(engine)

Session = sessionmaker(bind=engine)
session = Session()


class __User(pydantic.BaseModel):
    id: int
    username: str
    gender: str


users: typing.List[__User] = []

### наполняем базу рандомными сущностями
@app.get('/users/fill')
def fill(qty:int = 10):
    fill_base(session, qty)


### Передаю в FastAPI для корректного отображения Example Value Schema в /docs
@app.get("/users", response_model=typing.List[__User])
def get_users():
    users= []
    q = session.query(User).all()
    for user in q:
        new_user = __User(id=user.id, username=user.name, gender=user.gender)
        users.append(new_user)
    return users

@app.get("/users/search")
def get_users_gender(gender:str, limit=5):
    users= []
    q = session.query(User).filter(User.gender == gender).limit(limit)
    for user in q:
        new_user = __User(id=user.id, username=user.name, gender=user.gender)
        users.append(new_user)
    return users


@app.post('/users', response_model=typing.List[__User])
def create_user(user: __User):
    new_user = User(name=user.username, gender=user.gender)
    session.add(new_user)
    session.commit()
    user = __User(id=new_user.id, username=user.username, gender=user.gender)
    response = [user]
    return response

